﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.Attributes
{
    public class Permission : Attribute
    {
        public string Tag { get; set; }
        public string Description { get; set; }
        public Permission(string tag, string description = "")
        {
            Tag = tag;
            Description = description;
        }
    }
}
