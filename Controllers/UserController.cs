﻿using CrmServer.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using СherryLib.Controllers;
using VM = CrmServer.ViewModel;

namespace CrmServer.Controllers
{
    [Route("/api/v1/users")]
    [ApiController]
    public class UserController : CherryControllerBase<IUserService, VM.Request.UserModel, VM.Response.UserModel, int>
    {
        public UserController(IUserService service) : base(service)
        {
        }
    }
}
