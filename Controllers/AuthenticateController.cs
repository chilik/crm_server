﻿using AutoMapper;
using CrmServer.Data.Models;
using VM = CrmServer.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CrmServer.Controllers
{
    [Route("api/v1/auth")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;  
        private readonly IConfiguration _configuration;  
  
        public AuthenticateController(UserManager<User> userManager, IConfiguration configuration, IMapper mapper) : base()
        {  
            _userManager = userManager;  
            _configuration = configuration;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] VM.Request.LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.UtcNow.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] VM.Request.UserModel model)
        {
            try
            {
                var userExists = await _userManager.FindByNameAsync(model.Username);
                if (userExists != null)
                    throw new Exception("Username has already been taken!");

                var user = _mapper.Map<User>(model);
                user.SecurityStamp = Guid.NewGuid().ToString();

                var result = await _userManager.CreateAsync(user, model.Password);
                if (!result.Succeeded)
                    throw new Exception("User creation failed! Please check user details and try again.");

                return StatusCode(StatusCodes.Status200OK, new { Status = "Success", Message = "User created successfully!" });
            }catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", ex.Message });
            }
        }

        [Authorize]
        [HttpGet]
        [Route("secret-data")]
        public async Task<IActionResult> SecretData()
        {
            return Ok(new { Status = "Success", Message = "SECRET DATA!" });
        }
    }
}
