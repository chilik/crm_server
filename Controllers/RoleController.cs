﻿using CrmServer.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using СherryLib.Controllers;
using VM = CrmServer.ViewModel;

namespace CrmServer.Controllers
{
    
    [ApiController]
    [Route("api/v1/roles")]
    public class RoleController : CherryControllerBase<IRoleService, VM.Request.RoleModel, VM.Response.RoleModel, int>
    {
        public RoleController(IRoleService service) : base(service)
        {
        }
    }
}
