﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.Data.Models
{
    public class Role : IdentityRole<int>
    {
        public uint AccessLevel { get; set; }
        public IList<Permission> Permissions { get; set; }
    }
}
