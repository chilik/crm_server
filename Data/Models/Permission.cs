﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.Data.Models
{
    public class Permission
    {
        public Guid Id { get; set; }
        public string Tag { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
        public int RoleId { get; set; }
    }
}
