﻿using AutoMapper;
using CrmServer.Data.Models;
using CrmServer.ViewModel;
using VM = CrmServer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.Data.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<VM.Request.UserModel, User>();
            CreateMap<User, VM.Response.UserModel>();

            CreateMap<VM.Request.RoleModel, Role>();
            CreateMap<Role, VM.Response.RoleModel>();
        }
    }
}
