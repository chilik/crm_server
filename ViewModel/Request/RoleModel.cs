﻿using CrmServer.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using СherryLib.Models;

namespace CrmServer.ViewModel.Request
{
    public class RoleModel : IRequestModel
    {
        public string Name { get; set; }
        public uint AccessLevel { get; set; }
    }
}
