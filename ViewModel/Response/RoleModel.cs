﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using СherryLib.Models;

namespace CrmServer.ViewModel.Response
{
    public class RoleModel : IResponseModel
    {
        public string Name { get; set; }
        public uint AccessLevel { get; set; }
    }
}
