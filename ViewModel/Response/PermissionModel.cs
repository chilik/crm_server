﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.ViewModel.Response
{
    public class PermissionModel
    {
        public string Tag { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
    }
}
