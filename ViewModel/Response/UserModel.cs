﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using СherryLib.Models;

namespace CrmServer.ViewModel.Response
{
    public class UserModel : IResponseModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}
