﻿using AutoMapper;

namespace CrmServer.Services.Base
{
    public abstract class BaseService
    {
        protected readonly IMapper mapper;

        public BaseService(IMapper mapper)
        {
            this.mapper = mapper;
        }
    }
}
