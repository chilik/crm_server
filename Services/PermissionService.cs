﻿using AutoMapper;
using CrmServer.Data.Models;
using CrmServer.Services.Base;
using CrmServer.Services.Interfaces;
using VM = CrmServer.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmServer.Exceptions;
using CrmServer.Utils;
using CrmServer.Data;
using CrmServer.Attributes;

namespace CrmServer.Services
{
    public class PermissionService : BaseService, IPermissionService
    {
        private readonly ICurrentUserService currentUser;
        private readonly RoleManager<Role> roleManager;

        private readonly ApplicationDbContext context;

        public PermissionService(IMapper mapper, ICurrentUserService currentUser, RoleManager<Role> roleManager, ApplicationDbContext context) : base(mapper)
        {
            this.currentUser = currentUser;
            this.roleManager = roleManager;
            this.context = context;
        }

        [Attributes.Permission("permission.disable")]
        public async Task Disable(Guid id)
        {
            await SwithIsEnabledPermission(id, false);
        }

        [Attributes.Permission("permission.enable")]
        public async Task Enable(Guid id)
        {
            await SwithIsEnabledPermission(id, true);
        }

        public async Task<IEnumerable<VM.Response.PermissionModel>> GetByRoleId(int roleId)
        {
            var role = await roleManager.FindByIdAsync(roleId.ToString());
            if (role == null) throw new Exception("Role not found");

            var currentRole = await currentUser.GetCurrentRole();
            if (currentRole.AccessLevel < role.AccessLevel)
                return mapper.Map<IEnumerable<VM.Response.PermissionModel>>(role.Permissions);
            else
                throw new PermissionException("Access denied");
        }

        public async Task SeedPermissionsInRole(int roleId, bool defaultSeed = false)
        {
            var role = await roleManager.FindByIdAsync(roleId.ToString());
            if (role == null) throw new Exception("Role not found");

            Role currentRole = null;
            if (!defaultSeed)
            {
                currentRole = await currentUser.GetCurrentRole();
            }
            var attrPermissions = (Attributes.Permission[]) Attribute.GetCustomAttributes(typeof(Attributes.Permission));
            
            foreach(var permission in attrPermissions)
            {
                var isEnabled = defaultSeed;
                if (!defaultSeed) {
                    var currentPermission = currentRole.Permissions.Where(x => x.Tag == permission.Tag).FirstOrDefault();
                    isEnabled = currentPermission != null && currentPermission.IsEnabled;
                }

                role.Permissions.Add(new Data.Models.Permission
                {
                    Description = permission.Description,
                    Tag = permission.Tag,
                    IsEnabled = isEnabled
                });
            }

            context.Roles.Update(role);
            context.SaveChanges();
        }

        private async Task SwithIsEnabledPermission(Guid id, bool isEnabled)
        {
            var permission = context.Permissions.Find(id);
            if (permission == null) throw new Exception("Permission not found");
            var role = await roleManager.FindByIdAsync(permission.RoleId.ToString());
            if (role == null) throw new Exception("Role not found");
            var currentRole = await currentUser.GetCurrentRole();
            if (currentRole.AccessLevel < role.AccessLevel)
            {
                var currentPermission = currentRole.Permissions.Where(x => x.Tag == permission.Tag).FirstOrDefault();
                if (currentPermission != null && currentPermission.IsEnabled)
                {
                    permission.IsEnabled = isEnabled;
                    context.Permissions.Update(permission);
                    context.SaveChanges();
                }
                else
                    throw new PermissionException("Access denied");
            }
            else
                throw new PermissionException("Access denied");
        }
    }
}
