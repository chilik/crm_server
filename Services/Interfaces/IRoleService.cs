﻿using СherryLib.Services;
using VM = CrmServer.ViewModel;

namespace CrmServer.Services.Interfaces
{
    public interface IRoleService : IBaseCrud<VM.Request.RoleModel, VM.Response.RoleModel, int>, IService
    {
    }
}
