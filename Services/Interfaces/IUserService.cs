﻿using СherryLib.Services;
using VM = CrmServer.ViewModel;

namespace CrmServer.Services.Interfaces
{
    public interface IUserService : IBaseCrud<VM.Request.UserModel, VM.Response.UserModel, int>, IService
    {
    }
}
