﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using СherryLib.Services;
using VM = CrmServer.ViewModel;

namespace CrmServer.Services.Interfaces
{
    interface IPermissionService : IService
    {
        Task<IEnumerable<VM.Response.PermissionModel>> GetByRoleId(int roleId);
        Task SeedPermissionsInRole(int roleId, bool defaultSeed = false);
        Task Enable(Guid id);
        Task Disable(Guid id);
    }
}
