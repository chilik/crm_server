﻿using AutoMapper;
using CrmServer.Data.Models;
using CrmServer.Services.Base;
using CrmServer.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VM = CrmServer.ViewModel;

namespace CrmServer.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly UserManager<User> manager;

        public UserService(IMapper mapper, UserManager<User> manager) : base(mapper)
        {
            this.manager = manager;
        }

        public async Task<VM.Response.UserModel> Create(VM.Request.UserModel model)
        {
            var user = await manager.FindByNameAsync(model.Username);
            if (user != null)
                throw new Exception("Username has already been taken!");

            user = mapper.Map<User>(model);
            user.SecurityStamp = Guid.NewGuid().ToString();

            var result = await manager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                throw new Exception("User creation failed! Please check user details and try again.");

            return mapper.Map<VM.Response.UserModel>(user);
        }

        public async Task Delete(int id)
        {
            var user = await manager.FindByIdAsync(id.ToString());
            if (user == null)
                throw new Exception("User not found!");
            await manager.DeleteAsync(user);
        }

        public async Task<IEnumerable<VM.Response.UserModel>> GetAll()
        {
            return mapper.Map<IEnumerable<VM.Response.UserModel>>(manager.Users);
        }

        public async Task<VM.Response.UserModel> GetById(int id)
        {
            var user = await manager.FindByIdAsync(id.ToString());
            if (user == null)
                throw new Exception("User not found!");
            return mapper.Map<VM.Response.UserModel>(user);
        }

        public async Task<VM.Response.UserModel> Update(int id, VM.Request.UserModel model)
        {
            var user = await manager.FindByIdAsync(id.ToString());
            if (user == null)
                throw new Exception("User not found!");
            var result = await manager.UpdateAsync(mapper.Map(model, user));
            if (result.Succeeded) 
                return mapper.Map<VM.Response.UserModel>(user);
            else 
                throw new Exception(result.Errors.ToString());
        }
    }
}
