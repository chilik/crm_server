﻿using AutoMapper;
using CrmServer.Attributes;
using CrmServer.Data;
using CrmServer.Data.Models;
using CrmServer.Services.Base;
using CrmServer.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VM = CrmServer.ViewModel;

namespace CrmServer.Services
{
    public class RoleService : BaseService, IRoleService
    {

        private readonly RoleManager<Role> roleManager;
        public RoleService(IMapper mapper, RoleManager<Role> roleManager) : base(mapper)
        {
            this.roleManager = roleManager;
        }

        [Attributes.Permission("role.create")]
        public async Task<VM.Response.RoleModel> Create(VM.Request.RoleModel model)
        {
            var role = mapper.Map<Role>(model);
            var result = await roleManager.CreateAsync(role);
            if (result.Succeeded)
                return mapper.Map<VM.Response.RoleModel>(roleManager.FindByNameAsync(model.Name).Result);
            else
                throw new Exception(result.Errors.ToString());
        }

        [Attributes.Permission("role.delete")]
        public async Task Delete(int id)
        {
            var role = roleManager.FindByIdAsync(id.ToString()).Result;
            if (role != null)
                await roleManager.DeleteAsync(role);
            else
                throw new Exception("Role not found");
        }

        [Attributes.Permission("role.getall")]
        public async Task<IEnumerable<VM.Response.RoleModel>> GetAll()
        {
            return mapper.Map<IEnumerable<VM.Response.RoleModel>>(roleManager.Roles);
        }

        [Attributes.Permission("role.getbyid")]
        public async Task<VM.Response.RoleModel> GetById(int id)
        {
            var role = roleManager.FindByIdAsync(id.ToString()).Result;
            if (role != null)
                return mapper.Map<VM.Response.RoleModel>(role);
            else
                throw new Exception("Role not found");
        }

        [Attributes.Permission("role.update")]
        public async Task<VM.Response.RoleModel> Update(int id, VM.Request.RoleModel model)
        {
            var role = await roleManager.FindByIdAsync(id.ToString());
            if (role != null) {
                var result = await roleManager.UpdateAsync(mapper.Map(model, role));
                if (result.Succeeded) return mapper.Map<VM.Response.RoleModel>(role);
                else throw new Exception(result.Errors.ToString());
            }
            else
                throw new Exception("Role not found");
        }
    }
}
