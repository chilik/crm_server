﻿using CrmServer.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.Utils
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly IHttpContextAccessor httpContextAccessor;

        public CurrentUserService(UserManager<User> userManager, RoleManager<Role> roleManager, IHttpContextAccessor httpContextAccessor)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<Role> GetCurrentRole()
        {
            var currentUser = await GetCurrentUser();
            var currentUserRoleName = (await userManager.GetRolesAsync(currentUser)).FirstOrDefault();
            var currentUserRole = await roleManager.FindByNameAsync(currentUserRoleName);
            if (currentUserRole == null) throw new Exception("Current user role not found");
            return currentUserRole;
        } 

        public async Task<User> GetCurrentUser()
        {
            var currentUser = await userManager.FindByNameAsync(httpContextAccessor.HttpContext.User.Identity.Name);
            if (currentUser == null) throw new Exception("Current user not found");
            return currentUser;
        }
    }
}
