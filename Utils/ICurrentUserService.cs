﻿using CrmServer.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrmServer.Utils
{
    public interface ICurrentUserService
    {
        Task<User> GetCurrentUser();
        Task<Role> GetCurrentRole();
    }
}
